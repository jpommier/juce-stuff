#pragma once

#include <juce.h>

struct FocusDebugger : public juce::Component, public juce::Timer, public juce::DeletedAtShutdown {
  juce::WeakReference<juce::Component> currently_focused_component;
  juce::Rectangle<int> outline_r;

  FocusDebugger() {
    startTimer(50);
    setInterceptsMouseClicks(false, false);
    setOpaque(false);
    setAlwaysOnTop(true);
  }

  void updateMe() {
    if (currently_focused_component) {
      auto r = currently_focused_component->getScreenBounds().expanded(2);
      outline_r = r.withZeroOrigin();
      if (r.getWidth() < 300) r = r.withWidth(300);
      r = r.withHeight(r.getHeight() + 35);
      setBounds(r);
      if (!isVisible()) {
        if (!isOnDesktop()) {
          addToDesktop(juce::ComponentPeer::windowIsTemporary | juce::ComponentPeer::windowIgnoresMouseClicks |
                       juce::ComponentPeer::windowIgnoresKeyPresses);
        }
        setVisible(true);
      }
      repaint();
    } else {
      setVisible(false);
    }
  }

  void paint(juce::Graphics &g) override {
    //g.fillAll(Colours::yellow.withAlpha(0.1f));
    juce::Colour outline_colour(0xffff0000);
    g.setColour(outline_colour);
    g.drawRect(outline_r);
    g.setFont(juce::Font(11.f));
    juce::String txt = "null";
    juce::Component *c = currently_focused_component;
    if (c) {
      std::string classname = typeid(*c).name(); //cxxDemangle(typeid(*c).name());
      classname = classname.substr(0, 30);
      txt = "";
      txt << "class=" << classname << "; name=\"" << c->getName() << "\"; ID=\"" << c->getComponentID() << "\"";
      txt <<"; focOrder=" << c->getExplicitFocusOrder();
    }
    g.setColour(juce::Colours::black.withAlpha(0.6f));
    g.fillRect(0, outline_r.getBottom(), getWidth(), getHeight() - outline_r.getBottom());
    g.setColour(juce::Colours::white);
    g.drawFittedText(txt, getLocalBounds(), juce::Justification::bottomLeft, 4);
  }

  void updateCurrentFocusedComponent() {
    auto *c = juce::Component::getCurrentlyFocusedComponent();
    if (c != currently_focused_component) { currently_focused_component = c; }
  }

  void timerCallback() override {
    updateCurrentFocusedComponent();
    updateMe();
  }

  static void enableFocusDebugger(bool b) {
    static FocusDebugger *instance = nullptr;
    if (instance == nullptr && b) {
      instance = new FocusDebugger();
    } else if (instance && !b)
      delete instance;
  }
};

