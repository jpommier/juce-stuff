#pragma once
#include <juce.h>

class IOSPopupMenu {
public:
  struct ExtraOptions {
    Colour backgroundColour = Colours::transparentBlack;
    Colour textColour = Colours::transparentBlack;
  };

  ExtraOptions extra_options;
  IOSPopupMenu() : IOSPopupMenu("") {}
  IOSPopupMenu(const String &title);
  IOSPopupMenu(const PopupMenu &m);
  ~IOSPopupMenu();
  void addItem(int id, const String &text, bool isEnabled=true, bool isTicked=false);
  void addSubMenu(const String &subMenuName, const IOSPopupMenu &subMenu);
  void addSeparator();
  void addSectionHeader(const String &text);
  void addItemsFrom(const PopupMenu &m);
  /** use transparent colours to use the iOS default colours */
  void setExtraOptions(const ExtraOptions &options);
  /** note: options must either use set a parent component or a target component, that will be used to figure the
      toplevel UIViewController. This is absolutely unavoidable for AUv3. */
  void showMenuAsync(const PopupMenu::Options &options,  ModalComponentManager::Callback *callback);
  struct Impl;
private:
  std::unique_ptr<Impl> impl;
};
