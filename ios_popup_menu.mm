#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#include "ios_popup_menu.h"

@class IOSPopupMenu_TableViewController;

class IOSPopupMenu::Impl {
 public:

    template <typename T> class OwnedPtr {
        std::unique_ptr<T> ptr;
    public:
        operator T&() { return *ptr; }
        operator const T&() const { return *ptr; }
        OwnedPtr() { ptr = std::make_unique<T>(); }
        OwnedPtr(const OwnedPtr &other) : OwnedPtr(*other.ptr) {}
        OwnedPtr(const T &other) { ptr = std::make_unique<T>(other); }
        OwnedPtr &operator=(const T &other) { ptr = std::make_unique<T>(other); return *this; }
        ~OwnedPtr() {}
    };


  enum class ItemKind { Standard, SubMenu, Separator, Header };
  struct Item {
    int id = 0;
    ItemKind kind;
    String label;
    OwnedPtr<Array<Item>> subMenu;
    bool isTicked = false;
    bool isEnabled = true;
    bool hasSeparator = false;

    int internal_index = 0;

    Item(ItemKind k) : kind(k) {}
  };
  Array<Item> items;
  String title;

  IOSPopupMenu::ExtraOptions extra_options;

  UINavigationController *navCtrl = nullptr;

  Impl(const String &title) : title(title) {}
  ModalComponentManager::Callback *callback = nullptr;
  int result = -1;
  bool resultReceived = false;

  void addItem(int id, const String& text, bool isEnabled, bool isTicked) {
    Item item(ItemKind::Standard);
    item.id = id; item.label = text;
    item.isEnabled = isEnabled; item.isTicked = isTicked;
    addItem(item);
  }

  void addSubMenu(const String &subMenuName, const IOSPopupMenu &subMenu) {
    Item item(ItemKind::SubMenu);
    item.label = subMenuName;
    item.subMenu = subMenu.impl->items;
    addItem(item);
  }


  void addSeparator() {
    if (!items.isEmpty()) items.getReference(items.size()-1).hasSeparator = true;
    /*
    Item item(ItemKind::Separator);
    addItem(item);*/
  }

  void addSectionHeader(const String &text) {
    Item item(ItemKind::Header);
    item.label = text;
    addItem(item);
  }

  void showMenuAsync(const PopupMenu::Options &options,  ModalComponentManager::Callback *callback_) {
    resultReceived = false; callback = callback_;
    displayMenu(options);
  }

  void addItem(Item &item) {
    item.internal_index = items.size();
    items.add(item);
  }

  struct Section {
    String label;
    Array<const Item*> rows;
  };

  /* build the list of sections for the sub-menu found at the given path */
  Array<Section> getSections(const Array<int> &path) {
    Array<Section> sections;
    const Array<Item> *menu = &items;
    /* find the subbmenu that we are looking for, if the path is non-empty */
    for (int i : path) {
      if (i < menu->size() && menu->getReference(i).kind == ItemKind::SubMenu) {
        menu = &(static_cast<const Array<Item>&>(menu->getReference(i).subMenu));
      } else {
        menu = nullptr; break;
      }
    }
    if (menu) {
      Section sec;
      for (int row = 0; row < menu->size(); ++row) {
        const Item &it = menu->getReference(row);
        bool add_section = true;
        if (it.kind == ItemKind::Standard || it.kind == ItemKind::SubMenu) {
          if (sec.rows.size() < 3000)
            sec.rows.add(&it);
          if (row + 1 < menu->size()) add_section = false;
        }
        if (add_section && sec.rows.size()) { sections.add(sec); sec.label = ""; sec.rows.clear(); }

        if (it.kind == ItemKind::Header) sec.label = it.label;
        if (it.kind == ItemKind::Separator) sec.label = "";
      }
    }
    return sections;
  }
  void rowClicked(int section, int row, const Array<int> &path, IOSPopupMenu_TableViewController *tbv);
  void displayMenu(const PopupMenu::Options &);
};

@interface IOSPopupMenu_TableViewController : UITableViewController
{
  IOSPopupMenu::Impl *owner;
  Array<int> path;
  Array<IOSPopupMenu::Impl::Section> sections;
}
@end

@interface IOSPopupMenu_TableViewController ()
- (id)initWithOwner:(IOSPopupMenu::Impl*)owner_ path:(Array<int>)path_;
-(void)onTapCancel:(id)sender;
@end

void IOSPopupMenu::Impl::displayMenu(const PopupMenu::Options &options) {

  UIViewController *topCtrl = getTopLevelUIViewController(options.getTargetComponent() ? options.getTargetComponent() : options.getParentComponent());
  IOSPopupMenu_TableViewController *tableCtrl = [[IOSPopupMenu_TableViewController alloc ] initWithOwner:this path:Array<int>()];

  navCtrl = [[UINavigationController alloc] initWithRootViewController:tableCtrl];
  tableCtrl.navigationItem.title = juceStringToNS(title);


  UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:tableCtrl action:@selector(onTapCancel:)];
  tableCtrl.navigationItem.leftBarButtonItem = cancelBtn;

  UIViewController *ctrl = navCtrl ? navCtrl : tableCtrl;

  bool usePopover = true; // on ipad this will appear as a popover, while it appears as fullscreen table view on iphone
  if (!usePopover) {
    [topCtrl presentViewController: ctrl
                          animated: YES
                        completion: nil];
  } else {
    [ctrl setModalPresentationStyle:UIModalPresentationPopover];

    UIPopoverPresentationController *popPresenter = [ctrl popoverPresentationController];
    popPresenter.sourceView = topCtrl.view;

    Rectangle<int> targetArea = options.getTargetScreenArea();
    if (Component *c = options.getTargetComponent()) {
      targetArea = c->localAreaToGlobal(c->getLocalBounds());
    }

    popPresenter.sourceRect = CGRectMake (targetArea.getX() - topCtrl.view.bounds.origin.x, targetArea.getY() - topCtrl.view.bounds.origin.y, targetArea.getWidth(), targetArea.getHeight());
    [topCtrl presentViewController:ctrl animated:YES completion:nil];
  }
}

void IOSPopupMenu::Impl::rowClicked(int section, int row, const Array<int> &path, IOSPopupMenu_TableViewController *tbv) {
  const auto sections = getSections(path);
  const Item *item = nullptr;
  if (section >= 0 && section < sections.size()) {
    const auto &s = sections.getReference(section);
    if (row < s.rows.size()) {
      item = s.rows.getReference(row);
    } else {
      NSLog(@"Inexistent row clicked !??");
      return;
    }
  }
  if (item && item->kind == ItemKind::SubMenu) {
    Array<int> sub_path = path; sub_path.add(item->internal_index);
    IOSPopupMenu_TableViewController *sub_tbv = [[IOSPopupMenu_TableViewController alloc ] initWithOwner:this path:sub_path];
    [navCtrl pushViewController:sub_tbv animated:YES];
    // make sure the popover adjusts its size according to the new tableview
    [sub_tbv.tableView layoutIfNeeded];
    navCtrl.preferredContentSize = CGSizeMake(-1,sub_tbv.tableView.contentSize.height);
  } else {
    if (callback != nullptr) {
      result = (item ? item->id : -1); resultReceived = true;
      MessageManager::getInstance()->callAsync([cb = callback, r = result]() { cb->modalStateFinished(r); });
      //callback->modalStateFinished(result);
    }
    [tbv dismissViewControllerAnimated:YES completion:nil];
  }
}

IOSPopupMenu::IOSPopupMenu(const String &title) : impl(new Impl(title)) {
}

IOSPopupMenu::IOSPopupMenu(const PopupMenu &m) : impl(new Impl("")) {
  addItemsFrom(m);
}

IOSPopupMenu::~IOSPopupMenu() = default;

void IOSPopupMenu::addItemsFrom(const PopupMenu &m) {
  PopupMenu::MenuItemIterator iterator(m, false);
  while (iterator.next())
    {
      PopupMenu::Item &item = iterator.getItem();
      if (item.subMenu) { IOSPopupMenu sub(*item.subMenu); addSubMenu(item.text, sub); }
      else if (item.isSeparator) addSeparator();
      else if (item.isSectionHeader) addSectionHeader(item.text);
      else addItem(item.itemID, item.text, item.isEnabled, item.isTicked);
    }
}

void IOSPopupMenu::addItem(int id, const String &text, bool isEnabled, bool isTicked) {
  impl->addItem(id, text, isEnabled, isTicked);
}

void IOSPopupMenu::addSubMenu(const String &subMenuName, const IOSPopupMenu &subMenu) {
  impl->addSubMenu(subMenuName, subMenu);
}

void IOSPopupMenu::addSeparator() {
  impl->addSeparator();
}

void IOSPopupMenu::addSectionHeader(const String &text) {
  impl->addSectionHeader(text);
}

void IOSPopupMenu::showMenuAsync(const PopupMenu::Options &options,  ModalComponentManager::Callback *callback) {
  impl->extra_options = extra_options;
  impl->showMenuAsync(options, callback);
  impl.release();
}

inline UIColor *juceColourToUIColor(Colour c) {
  return [UIColor colorWithRed:c.getFloatRed() green:c.getFloatGreen() blue:c.getFloatBlue() alpha:c.getFloatAlpha()];
}

@implementation IOSPopupMenu_TableViewController
- (id)initWithOwner:(IOSPopupMenu::Impl*)owner_ path:(Array<int>)path_
{
  self = [super initWithStyle:UITableViewStylePlain];
  if (self) {
    self->owner = owner_;
    self->path = path_;
    self->sections = owner->getSections(path);
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  const auto &ex = owner->extra_options;
  if (!ex.backgroundColour.isTransparent()) {
    self.tableView.backgroundColor = juceColourToUIColor(ex.backgroundColour);
  }
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  if (self.navigationController)
    {
      /*UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"Push" style:UIBarButtonItemStylePlain target:self action:@selector(onPush)];
        self.navigationItem.rightBarButtonItem = item;
        self.navigationItem.title = NSStringFromCGSize(self.preferredContentSize);
        self.view.backgroundColor = [UIColor lightGrayColor];*/
      [self.tableView layoutIfNeeded];
      /*NSLog(@"viewWillAppear: setting preferredContentSize to %@ sections: %d nrows: %ld", NSStringFromCGSize(self.navigationController.preferredContentSize),
        (int)sections.size(), (long)[self tableView:self.tableView numberOfRowsInSection:0]);*/
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return sections.size();
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
  if (section < sections.size()) {
    String lbl = sections.getReference((int)section).label;
    return juceStringToNS((lbl == "" && section > 0) ? " " : lbl);
  }
  else return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
  const auto &ex = owner->extra_options;
  UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
  if (!ex.textColour.isTransparent()) {
    header.textLabel.textColor = juceColourToUIColor(ex.textColour);
  }
  if (!ex.backgroundColour.isTransparent()) {
    // Set the background color of our header/footer.
    header.contentView.backgroundColor = juceColourToUIColor(ex.backgroundColour.brighter());
  }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  if (section < sections.size()) {
    // recognize sections starting with a separator (does not really exist
    // in UITableView, so this is a bit hackish
    if (section > 0 && sections.getReference((int)section).label == "") {
      return 1;
    }
  }
  //cerr << "heightForHeaderInSection(" << section << "), return: " << [super tableView:tableView heightForHeaderInSection:section] << "\n";
  return UITableViewAutomaticDimension; //[super tableView:tableView heightForHeaderInSection:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  if (section < sections.size()) {
    return sections.getReference((int)section).rows.size();
  } else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{

  static NSString *CellIdentifier = @"Cell";
  UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

  const IOSPopupMenu::Impl::Item *item = nullptr;
  if (indexPath.section < sections.size() &&
      indexPath.row < sections.getReference((int)indexPath.section).rows.size()) {
    item = sections.getReference((int)indexPath.section).rows.getReference((int)indexPath.row);
  }

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
  }
  if (item) {
    if (item->kind == IOSPopupMenu::Impl::ItemKind::SubMenu) {
      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if (item->isTicked) {
      cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    if (!item->isEnabled) {
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
      cell.textLabel.enabled = false;
      cell.userInteractionEnabled = false;
    }
    cell.textLabel.text = juceStringToNS(item->label);

    if (item->hasSeparator) {
      //cell.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
      /*NSInteger separatorHeight = 2;
      UIView * additionalSeparator = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-separatorHeight,1000,separatorHeight)];
      additionalSeparator.backgroundColor = [UIColor grayColor];
      [cell addSubview:additionalSeparator];
      cell.layoutMargins = UIEdgeInsetsMake(-20,-20,-20,-20);
      [cell sizeToFit];*/
      cell.separatorInset =UIEdgeInsetsMake(-20,-20,-20,-20);
    }
    const auto &ex = owner->extra_options;

    if (!ex.backgroundColour.isTransparent()) {
      //[cell setBackgroundColor: juceColourToUIColor(ex.backgroundColour)];
      cell.backgroundColor = juceColourToUIColor(ex.backgroundColour);
    }
    if (!ex.textColour.isTransparent()) {
      cell.textLabel.textColor = juceColourToUIColor(ex.textColour);
    }
    //cell.layoutMargins = UIEdgeInsetsZero;
  }

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 34;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
  UISwitch *switcher = [cell.accessoryView isKindOfClass:[UISwitch class]] ? (UISwitch*)cell.accessoryView : nullptr;
  if (switcher) {
    [switcher setOn:!switcher.on animated:YES];
  } else {
    owner->rowClicked((int)indexPath.section, (int)indexPath.row, path, self);
  }

}

-(void)onTapCancel:(id)sender { owner->rowClicked(-1, -1, Array<int>(), self); }

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
