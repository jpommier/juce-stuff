diff --git a/modules/juce_gui_basics/menus/juce_PopupMenu.cpp b/modules/juce_gui_basics/menus/juce_PopupMenu.cpp
index 262c10e3a..1b9c0663f 100644
--- a/modules/juce_gui_basics/menus/juce_PopupMenu.cpp
+++ b/modules/juce_gui_basics/menus/juce_PopupMenu.cpp
@@ -302,6 +302,7 @@ struct MenuWindow  : public Component
                 ApplicationCommandManager** manager, float parentScaleFactor = 1.0f)
        : Component ("menu"),
          parent (parentWindow),
+         parentItem (parentWindow ? parentWindow->currentChild : nullptr),
          options (std::move (opts)),
          managerOfChosenCommand (manager),
          componentAttachedTo (options.getTargetComponent()),
@@ -1252,6 +1253,7 @@ struct MenuWindow  : public Component
 
     //==============================================================================
     MenuWindow* parent;
+    Component::SafePointer<ItemComponent> parentItem; // parent->currentChild at time of creation of this window
     const Options options;
     OwnedArray<ItemComponent> items;
     ApplicationCommandManager** managerOfChosenCommand;
@@ -1287,8 +1289,7 @@ public:
             return;
 
         startTimerHz (20);
-        handleMousePosition (e.getScreenPosition());
-    }
+        handleMousePosition (source.getScreenPosition().roundToInt());    }
 
     void timerCallback() override
     {
@@ -1305,7 +1306,7 @@ public:
 
     bool isOver() const
     {
-        return window.reallyContains (window.getLocalPoint (nullptr, source.getScreenPosition()).roundToInt(), true);
+      return reallyContains (window.getLocalPoint (nullptr, source.getScreenPosition()).roundToInt());
     }
 
     MenuWindow& window;
@@ -1316,18 +1317,81 @@ private:
     double scrollAcceleration = 0;
     uint32 lastScrollTime, lastMouseMoveTime = 0;
     bool isDown = false;
+    bool isTouchDragging = false; Point<int> lastTouchDraggingPos;
+
+    bool lastMousePosIsValid = false;
+    bool hasMouseMovedOnce = false;
+
+    bool reallyContains(const Point<int> localMousePos) const {
+#if JUCE_POPUPMENU_PATCHES
+        auto globalMousePos = window.localPointToGlobal(localMousePos);
+        auto *c = Desktop::getInstance().findComponentAt(globalMousePos);
+        if (!c) return false;
+        c = c->getTopLevelComponent();
+        return (c == window.getTopLevelComponent()) && window.reallyContains(localMousePos, true);
+#else
+        return window.reallyContains(localMousePos, true);
+#endif
+    }
 
     void handleMousePosition (Point<int> globalMousePos)
     {
         auto localMousePos = window.getLocalPoint (nullptr, globalMousePos);
         auto timeNow = Time::getMillisecondCounter();
 
+#if JUCE_POPUPMENU_PATCHES
+        /* ref: https://forum.juce.com/t/improving-popupmenu-for-touch-screens/37784 */
+        if (source.isTouch()) {
+            if (source.getIndex() > 0) return; // one finger only
+
+            if (source.isDragging() && source.hasMovedSignificantlySincePressed()) {
+                bool move = isTouchDragging && window.canScroll();
+                // using globalMousePos does not work well when scrolling (jumps)
+                auto pos = source.getScreenPosition().roundToInt();
+
+                if (move) {
+                    auto delta_y = lastTouchDraggingPos.y - pos.y;
+                    if (delta_y) {
+                        window.alterChildYPos(delta_y);
+                    }
+                }
+                lastTouchDraggingPos = pos;
+                isTouchDragging = true;
+            }
+            if (!source.isDragging() && isTouchDragging) {
+                isTouchDragging = false;
+                isDown = false; // prevent triggerCurrentlyHighlightedItem from being called after a drag
+            }
+            if (isTouchDragging) {
+                if (window.activeSubMenu)
+                    window.activeSubMenu->hide (nullptr, true);
+                return; // skip everything else (the auto-scroll, the submenu display..)
+            }
+        }
+#endif // JUCE_POPUPMENU_PATCHES
+
         if (timeNow > window.timeEnteredCurrentChildComp + 100
-             && window.reallyContains (localMousePos, true)
+             && reallyContains (localMousePos)
              && window.currentChild != nullptr
              && ! (window.disableMouseMoves || window.isSubMenuVisible()))
         {
-            window.showSubMenuFor (window.currentChild);
+#if JUCE_POPUPMENU_PATCHES
+            if (window.activeSubMenu && window.activeSubMenu->parentItem == window.currentChild) {
+            } else {
+                // the test on hasMouseMovedOnce prevents immediately showing a submenu if the mouse
+                // cursor happens to be over a submenu item when the window appears.
+                // it is especially annoying on touchscreens
+                if (hasMouseMovedOnce)
+                  window.showSubMenuFor (window.currentChild);
+            }
+#else
+                window.showSubMenuFor (window.currentChild);
+#endif // JUCE_POPUPMENU_PATCHES
+        }
+
+        if (!lastMousePosIsValid) { lastMousePos = globalMousePos; lastMousePosIsValid = true; }
+        if (lastMousePosIsValid && lastMousePos != globalMousePos) {
+          hasMouseMovedOnce = true;
         }
 
         highlightItemUnderMouse (globalMousePos, localMousePos, timeNow);
@@ -1360,8 +1424,21 @@ private:
         else if (wasDown && timeNow > window.windowCreationTime + 250
                    && ! (isDown || overScrollArea))
         {
-            if (window.reallyContains (localMousePos, true))
+            if (reallyContains (localMousePos)) {
+ #if JUCE_POPUPMENU_PATCHES
+                // hide / show the submenu when clicking on its item (useful for small screens where a submenu
+                // may cover its parent menu)
+                if (window.activeSubMenu && window.activeSubMenu->parentItem == window.currentChild) {
+                    if (window.activeSubMenu->isVisible()) {
+                        if (timeNow > window.activeSubMenu->windowCreationTime + 250)
+                            window.activeSubMenu->hide(nullptr, true);
+                    } else {
+                        window.showSubMenuFor(window.currentChild);
+                    }
+                }
+#endif // JUCE_POPUPMENU_PATCHES
                 window.triggerCurrentlyHighlightedItem();
+            }
             else if ((window.hasBeenOver || ! window.dismissOnMouseUp) && ! isOverAny)
                 window.dismissMenu (nullptr);
 
@@ -1377,7 +1454,7 @@ private:
     {
         if (globalMousePos != lastMousePos || timeNow > lastMouseMoveTime + 350)
         {
-            const auto isMouseOver = window.reallyContains (localMousePos, true);
+            const auto isMouseOver = reallyContains (localMousePos);
 
             if (isMouseOver)
                 window.hasBeenOver = true;
diff --git a/modules/juce_gui_basics/native/juce_ios_UIViewComponentPeer.mm b/modules/juce_gui_basics/native/juce_ios_UIViewComponentPeer.mm
index adc0479fc..e13e6a594 100644
--- a/modules/juce_gui_basics/native/juce_ios_UIViewComponentPeer.mm
+++ b/modules/juce_gui_basics/native/juce_ios_UIViewComponentPeer.mm
@@ -911,8 +911,14 @@ static float getTouchForce (UITouch* touch) noexcept
 
         if (isUp (mouseEventFlags))
         {
+#if JUCE_POPUPMENU_PATCHES
+            /* ref: https://forum.juce.com/t/improving-popupmenu-for-touch-screens/37784 */
+            handleMouseEvent (MouseInputSource::InputSourceType::touch, pos, modsToSend,
+                              MouseInputSource::invalidPressure, MouseInputSource::invalidOrientation, time, {}, touchIndex);
+#else
             handleMouseEvent (MouseInputSource::InputSourceType::touch, MouseInputSource::offscreenMousePos, modsToSend,
                               MouseInputSource::invalidPressure, MouseInputSource::invalidOrientation, time, {}, touchIndex);
+#endif // JUCE_POPUPMENU_PATCHES
 
             if (! isValidPeer (this))
                 return;
